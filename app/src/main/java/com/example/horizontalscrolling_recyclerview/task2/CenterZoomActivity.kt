package com.example.horizontalscrolling_recyclerview.task2

import android.os.Bundle
import android.view.animation.AnimationUtils
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.PagerSnapHelper
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.SnapHelper
import com.example.horizontalscrolling_recyclerview.R


class CenterZoomActivity : AppCompatActivity() {
    // Initializing an empty ArrayList to be filled with animals
    private val animals: ArrayList<String> = ArrayList()
    private lateinit var recyclerView: RecyclerView
    private lateinit var viewAdapter: CenterZoomAdapter
    private lateinit var viewManager: CenterZoomLinearLayoutManager

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_center_zoom)

        // Loads animals into the ArrayList
        addAnimals()

        viewAdapter = CenterZoomAdapter(animals, this)

        viewManager = CenterZoomLinearLayoutManager(this)

        val snapHelper: SnapHelper = PagerSnapHelper()

        val animation = AnimationUtils.loadLayoutAnimation(this, R.anim.layout_anim)

        // Creates a vertical Layout Manager
        recyclerView = findViewById<RecyclerView>(R.id.recyclerView).apply {
            setHasFixedSize(true)
            layoutManager = viewManager
            adapter = viewAdapter
        }
        snapHelper.attachToRecyclerView(recyclerView)

    }

    // Adds animals to the empty animals ArrayList
    fun addAnimals() {
        animals.add("dog")
        animals.add("cat")
        animals.add("owl")
        animals.add("cheetah")
        animals.add("raccoon")
        animals.add("bird")
        animals.add("snake")
        animals.add("lizard")
        animals.add("hamster")
        animals.add("bear")
        animals.add("lion")
        animals.add("tiger")
        animals.add("horse")
        animals.add("frog")
        animals.add("fish")
        animals.add("shark")
        animals.add("turtle")
        animals.add("elephant")
        animals.add("cow")
        animals.add("beaver")
        animals.add("bison")
        animals.add("porcupine")
        animals.add("rat")
        animals.add("mouse")
        animals.add("goose")
        animals.add("deer")
        animals.add("fox")
        animals.add("moose")
        animals.add("buffalo")
        animals.add("monkey")
        animals.add("penguin")
        animals.add("parrot")
    }
}