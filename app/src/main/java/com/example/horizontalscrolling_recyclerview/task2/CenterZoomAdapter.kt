package com.example.horizontalscrolling_recyclerview.task2

import android.animation.ObjectAnimator
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.horizontalscrolling_recyclerview.R
import kotlinx.android.synthetic.main.item_swipe.view.*


class CenterZoomAdapter : RecyclerView.Adapter<CenterZoomAdapter.MyViewHolder> {

    lateinit var list: List<String>
    private var lastPosition = -1
    private val context: Context
    private var DURATION = 500
    private var on_attach = true
    private val TAG = "SwipeAdapter"


    constructor(list: List<String>, context: Context) : super() {
        this.list = list
        this.context = context
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_center_zoom, parent, false)
        return MyViewHolder(view)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {

        holder.itemView.run {
            if (position == 0) {
//                tvAbout.text = "In this application we will learn about ViewPager2"
                ivImage.setImageResource(R.drawable.face_image)
            }
            if (position == 1) {
//                tvAbout.text = "In this application we will learn about ViewPager2"
                ivImage.setImageResource(R.drawable.face_image)
            }
            if (position == 2) {
//                tvAbout.text = "In this application we will learn about ViewPager2"
                ivImage.setImageResource(R.drawable.face_image)
            }
            if (position == 3) {
//                tvAbout.text = "In this application we will learn about ViewPager2"
                ivImage.setImageResource(R.drawable.face_image)
            }

        }
    }


    class MyViewHolder : RecyclerView.ViewHolder {

        constructor(itemView: View) : super(itemView) {

        }

        private fun deScaleView(v: View?) {
            v?.let {
                v.clearAnimation()
                val scaleAnimation = ObjectAnimator.ofFloat(v, "scaleY", 1f)
                scaleAnimation.duration = 50
                scaleAnimation.start()
            }
        }

        private fun scaleView(v: View?) {
            v?.let {
                v.clearAnimation()
                val scaleAnimation = ObjectAnimator.ofFloat(v, "scaleY", 1.25f)
                scaleAnimation.duration = 50
                scaleAnimation.start()
            }
        }

        fun animate() {
            itemView.onFocusChangeListener = View.OnFocusChangeListener { v, isFocused ->
                if (isFocused) {
                    scaleView(v);
                } else {
                    deScaleView(v);
                }
            }
        }
    }

}