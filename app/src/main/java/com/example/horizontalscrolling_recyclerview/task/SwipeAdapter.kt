package com.example.horizontalscrolling_recyclerview.task

import android.animation.AnimatorSet
import android.animation.ObjectAnimator
import android.content.Context
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.view.View.OnFocusChangeListener
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import androidx.recyclerview.widget.RecyclerView
import com.example.horizontalscrolling_recyclerview.R
import kotlinx.android.synthetic.main.item_swipe.view.*


class SwipeAdapter : RecyclerView.Adapter<SwipeAdapter.MyViewHolder> {

    lateinit var list: List<String>
    private var lastPosition = -1
    private val context: Context
    private var DURATION = 500
    private var on_attach = true
    private val TAG = "SwipeAdapter"


    constructor(list: List<String>, context: Context) : super() {
        this.list = list
        this.context = context
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_swipe, parent, false)
        return MyViewHolder(view)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {

        holder.itemView.run {
            if (position == 0) {
//                tvAbout.text = "In this application we will learn about ViewPager2"
                ivImage.setImageResource(R.drawable.face_image)
            }
            if (position == 1) {
//                tvAbout.text = "In this application we will learn about ViewPager2"
                ivImage.setImageResource(R.drawable.face_image)
            }
            if (position == 2) {
//                tvAbout.text = "In this application we will learn about ViewPager2"
                ivImage.setImageResource(R.drawable.face_image)
            }
            if (position == 3) {
//                tvAbout.text = "In this application we will learn about ViewPager2"
                ivImage.setImageResource(R.drawable.face_image)
            }

            holder.animate()
        }
    }

    fun addListener(root: View) {

        root.setOnGenericMotionListener(object : View.OnGenericMotionListener {
            override fun onGenericMotion(v: View?, p1: MotionEvent?): Boolean {
                v?.let {
                    // run scale animation and make it bigger
                    val anim =
                        AnimationUtils.loadAnimation(v.context, R.anim.scale_in)
                    v.startAnimation(anim)
                    anim.fillAfter = true
                }

                return true
            }

        })
        // bind focus listener
/*        root.tvAbout.setOnFocusChangeListener(object : OnFocusChangeListener {
            override fun onFocusChange(
                v: View,
                hasFocus: Boolean
            ) {
                if (hasFocus) {
                    // run scale animation and make it bigger
                    val anim =
                        AnimationUtils.loadAnimation(root.context, R.anim.scale)
                    root.startAnimation(anim)
                    anim.fillAfter = true
                } else {
                    // run scale animation and make it smaller
                    val anim =
                        AnimationUtils.loadAnimation(root.context, R.anim.scale_out)
                    root.startAnimation(anim)
                    anim.fillAfter = true
                }
            }
        })*/
    }

/*    private fun setAnimation(viewToAnimate: View, position: Int) {
        // If the bound view wasn't previously displayed on screen, it's animated
       *//* if (position-1 > lastPosition) {
            val animation = AnimationUtils.loadAnimation(context, R.anim.scale)
            viewToAnimate.startAnimation(animation)
            lastPosition = position
        }*//*

        if (position > lastPosition) {
            val anim = ObjectAnimator.ofFloat(viewToAnimate, "scaleY", 0.8f)
            anim.duration = 300 // duration 3 seconds

            anim.start()
        }
    }*/

    private fun setAnimation(itemView: View, i: Int) {
        var i = i
        if (!on_attach) {
            i = -1
        }
        val isNotFirstItem = i == -1
        i++
        itemView.alpha = 0f
        val animatorSet = AnimatorSet()
        val animator = ObjectAnimator.ofFloat(itemView, "alpha", 0f, 0.5f, 1.0f)
        ObjectAnimator.ofFloat(itemView, "alpha", 0f).start()
        animator.startDelay = (if (isNotFirstItem) DURATION / 2 else i * DURATION / 3).toLong()
        animator.duration = 500
        animatorSet.play(animator)
        animator.start()
    }

/*    override fun onAttachedToRecyclerView(recyclerView: RecyclerView) {
        recyclerView.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                Log.d(TAG, "onScrollStateChanged: Called $newState")
                on_attach = false
                super.onScrollStateChanged(recyclerView, newState)
            }
        })
        super.onAttachedToRecyclerView(recyclerView)
    }*/


    class MyViewHolder : RecyclerView.ViewHolder {

        constructor(itemView: View) : super(itemView) {

        }

        private fun deScaleView(v: View?) {
            v?.let {
                v.clearAnimation()
                val scaleAnimation = ObjectAnimator.ofFloat(v, "scaleY", 1f)
                scaleAnimation.duration = 50
                scaleAnimation.start()
            }
        }

        private fun scaleView(v: View?) {
            v?.let {
                v.clearAnimation()
                val scaleAnimation = ObjectAnimator.ofFloat(v, "scaleY", 1.25f)
                scaleAnimation.duration = 50
                scaleAnimation.start()
            }
        }

        fun animate() {
            itemView.onFocusChangeListener = View.OnFocusChangeListener { v, isFocused ->
                if (isFocused) {
                    scaleView(v);
                } else {
                    deScaleView(v);
                }
            }
        }
    }

}